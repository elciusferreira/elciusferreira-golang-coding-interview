package app

import (
	"bytes"
	"fmt"
	"net/http"
	"text/template"

	"github.com/labstack/echo/v4"

	"github.com/BEON-Tech-Studio/golang-live-coding-challenge/internal/models"
)

var htmlTemplates map[string]*template.Template

type params map[string]interface{}

func home(c echo.Context) error {
	return c.HTML(http.StatusOK, execTemplateFromBase("Home", "home", params{}))
}

func getStates(c echo.Context) error {
	var states []models.State

	result := db.Find(&states)
	if result.RowsAffected == 0 {
		statesData, err := FetchStates()
		if err != nil {
			return err
		}
		states = statesData.States
	}

	statesHtml := execTemplateFromBase("States", "states", params{
		"states":       states,
		"row_template": htmlTemplates["states-row"],
	})
	return c.HTML(http.StatusOK, statesHtml)
}

func getStatesJson(c echo.Context) error {
	name := c.QueryParam("name")

	fmt.Println(name)

	if name != "" {
		var state models.State
		fmt.Println("trying to get the state")
		result := db.Where("name = ?", name).Find(&state)

		fmt.Println("database query ran")

		if result.RowsAffected != 0 && result.Error != nil {
			fmt.Println("database query success")

			return c.JSON(http.StatusOK, state)
		}
	}

	fmt.Println("try to find all states in database")

	var states []models.State

	result := db.Find(&states)
	if result.RowsAffected != 0 && result.Error != nil {
		return c.JSON(http.StatusOK, states)
	}

	fmt.Println("try to find all states in api")

	statesData, err := FetchStates()
	if err != nil {
		return err
	}

	fmt.Println("try to find all states in api")

	err = c.Bind(statesData.States)

	fmt.Println("request made to the api")

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, statesData.States)
}

func getCategoriesJson(c echo.Context) error {
	var categories []models.Category

	if err := c.Bind(categories); err != nil {
		return err
	}

	return c.JSON(http.StatusOK, categories)
}

// Helpers

func execTemplateFromBase(title, templateName string, p params) string {
	return execTemplate(
		"base-template",
		params{
			"title": title,
			"body":  execTemplate(templateName, p),
		},
	)
}

func execTemplate(templateName string, p params) string {
	t := htmlTemplates[templateName]
	if t == nil {
		return ""
	}
	var res bytes.Buffer
	err := t.Execute(&res, p)
	if err != nil {
		fmt.Println("ERROR in execTemplate:", err.Error())
		return ""
	}
	return res.String()
}
