module github.com/BEON-Tech-Studio/golang-live-coding-challenge

go 1.15

require (
	github.com/githubnemo/CompileDaemon v1.4.0 // indirect
	github.com/go-resty/resty/v2 v2.6.0
	github.com/labstack/echo/v4 v4.6.1
	github.com/spf13/viper v1.9.0
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.16
)
